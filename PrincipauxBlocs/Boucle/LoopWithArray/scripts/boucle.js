const chats = ['Bill', 'Jeff', 'Pete', 'Biggles', 'Jasmine'];
const paragraphe = document.querySelector('p');

let info = "Mes chats s'appellent ";

for(let i = 0; i < chats.length; i++) {

    if(i === chats.length - 1) {
        info += 'and ' + chats[i] + '.';
    } else {
        info += chats[i] + ', ';   
    }
}

paragraphe.textContent = info;