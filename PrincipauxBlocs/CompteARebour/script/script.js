let i = 10;

while(i >= 0) {
    const para = document.createElement('p');
    if(i === 10) {
        para.textContent = 'Chrono : ' + i;
    } else if(i === 0) {
        para.textContent = 'BOOM !';
    } else {
        para.textContent = i;
    }

    document.body.appendChild(para);

    i--;
}