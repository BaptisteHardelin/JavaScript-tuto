
/**
 * Create a new Personne
 *
 * @class Personne
 */
class Personne {

    /**
     * Créer une nouvelle instance de Personne
     * @param {string} nom Le nom de la personne
     * @param {string} prenom Le prénom de la personne
     * @param {number} age L'age de la personne
     * @param {string[]} genre Le genre de la personne
     * @param {string[]} interets Les intérêts de la personne
     * @memberof Personne
     */
    constructor(nom, prenom, age, genre, interets) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.genre = genre;
    }
}
