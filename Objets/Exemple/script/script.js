var personne = {
    nom: {
        prenom: 'Jean',
        nomFamille: 'Dupont'
    },
    age: 32,
    sexe: 'Masculin',
    interet: ['musique', 'skier'],
    bio: function() {
        alert(this.nom.prenom + ' ' + this.nom.nomFamille + ' a ' + this.age + ' ans. Il aime ' + this.interet[0] + ' et ' + this.interet[1] + '.');
    },
    salutation: function() {
        alert('Bonjour ! Je suis ' + this.nom.prenom + '.');
    },
    yeux: 'noisette',
    auRevoir: function() {
        alert('Bye tout le monde !');
    }
};

console.log(personne.nom.prenom);
console.log(personne.nom.nomFamille);
console.log(personne.age);
console.log(personne.interet[1]);
console.log(personne.bio());
console.log(personne.salutation());
console.log(personne['yeux']);
console.log(personne.auRevoir());