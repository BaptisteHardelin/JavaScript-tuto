var select = document.querySelector('select');
var list = document.querySelector('ul');
var h1 = document.querySelector('h1');

select.addEventListener('change', createCalender());

select.onchange = function() {
    let choice = select.value;
    let days = 31;

    if(choice === 'February') {
        days = 28;
    } else if(choice === 'April' || choice === 'June' || choice === 'September' || choice === 'November') {
        days = 30;
    }


    createCalender(days, choice);
}

function createCalender(days, choice) {
    list.innerHTML = '';
    h1.textContent = choice;

    for(var i = 0; i <= days; i++) {
        const listItem = document.createElement('li');
        listItem.textContent = i;
        list.appendChild(listItem);
    }
}

createCalender(31, 'Janvier');