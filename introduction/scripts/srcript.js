document.addEventListener("DOMContentLoaded", function() {
  
  /**
   * Fonction : créer un nouveau paragraphe et l'ajouter au fichier html
   */
  function createParagraph() {
    let para = document.createElement('p');
    para.textContent = 'Vous avez cliqué sur le bouton!';
    document.body.appendChild(para);
  }

  /**
   * On selectionne tout les boutons de notre page
   */
  const buttons = document.querySelectorAll('button');

  /**
   * Quand on click sur un bouton présent sur la page, la fonction "createParagraph()" sera exécutée
   */
  for(let i = 0; i < buttons.length ; i++) {
    buttons[i].addEventListener('click', createParagraph);
  }
})
