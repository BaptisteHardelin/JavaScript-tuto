let name = 'Babaprog';

console.log("Longueur de la chaine : " + name + " : " + name.length);
console.log(name[0]); // Affiche la première lettre de name
console.log(name[name.length - 1]); // Affiche la dernière lettre de name
console.log(name.indexOf('prog')); // Regarde si il y a la sous-chaîne 'prog'
console.log(name.indexOf('progg')); // Affiche -1 car cette sous chaîne n'appartient pas à name
console.log(name.slice(0, 2)); // Récupère les lettres dans l'intervalle 0, 2 ou 2 n'est pas compris donc ->  [0, 2[

// Concaténation dans un contexte
let button = document.querySelector('button');

button.onclick = function() {
    let name = prompt('Quel est votre nom ?');
    alert('Hello ' + name + ', sympa de vous voir !');
};

let myDate = "20" + "02";
console.log(typeof myDate);
console.log(Number(myDate));
let number = 42;
console.log(typeof number.toString());

let cities = [
    'lonDon',
    'ManCHESTer',
    'BiRmiNGHAM',
    'liVERpoOL'
]
let upperCaseCities;

/**
 * On affiche les éléments d'upperCaseCities en majuscule
 */
for(let i = 0; i < cities.length; i++) {
    upperCaseCities = cities[i].toLocaleUpperCase();
    console.log(upperCaseCities);
}

console.log('\n');
console.log('\n');
console.log('\n');

var stations = [
    'MAN675847583748sjt567654;Manchester Piccadilly',
    'GNF576746573fhdg4737dh4;Greenfield',
    'LIV5hg65hd737456236dch46dg4;Liverpool Lime Street',
    'SYB4f65hf75f736463;Stalybridge',
    'HUD5767ghtyfyr4536dh45dg45dg3;Huddersfield'
];

let id;
let indexSemlicon;

let input;

for(let i = 0; i < stations.length; i++) {
    let res = stations[i];
    id = res.slice(0, 3);
    indexSemlicon = res.indexOf(";");
    let name = res.slice((indexSemlicon + 1));

    input = id + ": " + name;
    console.log(input);
}