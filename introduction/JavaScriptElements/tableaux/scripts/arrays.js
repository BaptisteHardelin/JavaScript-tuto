let shopping = ['pain', 'lait', 'fromage'];
shopping[0] = 'chocolat';
console.log(shopping[0]);
console.log(shopping.length);

for(var i = 0; i < shopping.length; i++) {
    console.log(shopping[i]);
}

console.log('\n');
console.log('\n');
console.log('\n');

let myData = 'Manchester,London,Liverpool,Birmingham,Leeds,Carlisle';
let myArray = myData.split(',');
console.log(myData);

myArray.push('Cardiff');
myArray.push('Bradford', 'Brighton');

let newLength = myArray.push('Bristol');
console.log('newLength : ' + newLength);

let removedItem = myArray.pop();
console.log('removedItem : ' + removedItem);

myArray.unshift('Edinburgh'); // Ajout en tête de tableau
let removedItem2 = myArray.shift(); // Retire les éléments en tête de tableau
console.log('removedItem2 : ' + removedItem2);

for(var i = 0; i < myArray.length; i++) {
    console.log(myArray[i]);
}


let myNewString = myArray.join(',');
console.log(myNewString);

console.log('\n');
console.log('\n');
console.log('\n');

let dogNames = ["Rocket","Flash","Bella","Slugger"];
console.log(dogNames.toString());

//https://developer.mozilla.org/fr/docs/Learn/JavaScript/First_steps/Silly_story_generator