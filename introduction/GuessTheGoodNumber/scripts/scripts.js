/**
 * Génération aléatoire d'un nombre
 *  Math.floor transforme les nombre en virgule en nombre simple ex : 5.6 = 6
 */
let randomNumber = Math.floor(Math.random() * 100) + 1;

/**
 * On récupère le paragraphe ppuis on ajout nos tentative à celui-ci
 */
let guesses = document.querySelector('.guesses');

/**
 * On récupère le paragraphe et on dit si c'est faux ou vrai
 */
let lastResult = document.querySelector('.lastResult');
/**
 * On récupère le paragraphe et on dit à l'utilisateur si la valeur est plus grande ou plus petite
 */
let lowOrHi = document.querySelector('.lowOrHi');

/**
 * On récupère l'input
 */
let guessSubmit = document.querySelector('.guessSubmit');
/**
 * On récupère la valeur du champ de texte
 */
let guessField = document.querySelector('.guessField');

/**
 * Notre compteur de tentatives
 */
let guessCount = 1;
/**
 * Notre bouton de reset
 */
let resetButton;

//Place automatiquement le cursor dans le champ texte
guessField.focus();

/**
 * Logique du jeu
 */
function checkGuess(){
  //On regarde si le nombre saisie est bien un nombre
  let userGuess = Number(guessField.value);
  // On affiche le text 'Propositions précédentes :' et on ajoute les propositions précdente à celui-ci
  guesses.textContent = 'Propositions précédentes : ';
  guesses.textContent += userGuess + ' ';

  /* Si on trouve le nombre alors on a un message de victoire plus le background color en vert
      et on finit le jeu
  */
  if (userGuess === randomNumber) {
    lastResult.textContent = 'Bravo, vous avez trouvé le nombre !';
    lastResult.style.backgroundColor = 'green';
    lowOrHi.textContent = '';
    setGameOver();
    //Si notre nombre de tentative est égale à 10 et qu'on a pas trouvé le bon nombre alors on a perdu...
  } else if (guessCount === 10) {
     lastResult.textContent = '!!! PERDU !!!';
     setGameOver();
     /**
      * Si on ne trouve pas le bon nombre un message 'Faux' plus le background color en rouge et
      * le programme nous dit si le nombre qu'on doit trouver est plus grand ou plus petit que le 
      * nombre qu'on a envoyé
      */
  } else {
     lastResult.textContent = 'Faux !';
     lastResult.style.backgroundColor = 'red';
     if (userGuess < randomNumber) {
      lowOrHi.textContent = 'Le nombre saisi est trop petit !';
     } else if (userGuess > randomNumber) {
      lowOrHi.textContent = 'Le nombre saisi est trop grand !';
     }
  }

  /**
   * On incrémente notre compteur de tentative
   */
  guessCount++;
  guessField.value = '';
  guessField.focus();
}

/**
 * Si la valeur du bouton n'est pas null alors on peut cliquer dessus et appeler la fonction checkGuess
 */
if(guessSubmit != null) {
  guessSubmit.addEventListener('click', checkGuess);
}

/**
 * Fonction qui met fin au jeu
 */
function setGameOver() {
  guessField.disabled = true;
  guessSubmit.disabled = true;
  resetButton = document.createElement('button');
  resetButton.textContent = 'Start new game';
  document.body.appendChild(resetButton);
  resetButton.addEventListener('click', resetGame);
}

/**
 * Fonction qui réinitialise tout les paramètres du jeu pour pouvoir y rejouer
 */
function resetGame() {
  guessCount = 1;

  let resetParas = document.querySelectorAll('.resultParas p');
  for (let i = 0 ; i < resetParas.length ; i++) {
    resetParas[i].textContent = '';
  }

  resetButton.parentNode.removeChild(resetButton);

  guessField.disabled = false;
  guessSubmit.disabled = false;
  guessField.value = '';
  guessField.focus();

  lastResult.style.backgroundColor = 'white';

  randomNumber = Math.floor(Math.random() * 100) + 1;
}